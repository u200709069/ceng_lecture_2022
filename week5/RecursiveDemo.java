public class RecursiveDemo {
	
	public static void main(String[] args){

		System.out.println(sumLoop(5));
		System.out.println(sumRec(5));
		System.out.println(factLoop(5));
		System.out.println(factRec(5));
	}

	public static int sumRec(int number) {

		if( number == 0)
			return 0;

		return sumRec(number - 1) + number;

	}

	public static int factRec(int number) {
		if( number == 0)
			return 1;
		return factRec(number - 1) * number;

	}
	public static int sumLoop(int number){
		int sum = 0;

		for (int i =0; i<=number; i++){
			sum+= i;
		}
		return sum;
	}


	public static int factLoop(int number){
		int f = 1;

		for (int i =1; i<=number; i++){
			f*= i;
		}
		return f;
	}
}