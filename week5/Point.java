public class Point {

    double xCoord;
    double yCoord;

    static int count = 0;

    public Point(){
        System.out.println("point is neing created");
        Point.count++;
    }

    public Point(double x, double y){
        System.out.println("point is neing created");
        xCoord = x;
        yCoord = y;
        count++;
    }

    public double distancefromOrigin(){
        return Math.sqrt(xCoord * xCoord + yCoord * yCoord);
    }

    public void move ( double xDist, double yDist){
        xCoord += xDist;
        yCoord += yDist;
    }

    public double distancefromPoint(Point p){
        return Math.sqrt(Math.pow(xCoord - p.xCoord,2) +
                Math.pow(yCoord - p.yCoord,2));
    }
}
