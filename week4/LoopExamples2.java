public class LoopExamples2 {
	
	public static void main(String[] args){

		for(int i = 0; i < 100; i++){
			if(i == 50)
				break;
			System.out.println("Rule #" + i);
		}


		out: for (int i = 0; i < 3; i++) {
			for (int j = 2; j < 5; j++) {
				if (j == 4)
					continue out;
				System.out.println (i + "," + j);
			}
			System.out.println("End of outer loop i = " + i);
		}


		System.out.println("Method Ends");
	}




}