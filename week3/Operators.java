public class Operators {
	
	public static void main(String[] args){

		int i = 5;

		boolean bool  = i++ == 5 || i++ == 6;

		i += 1;  // i = i + 5
		++i;     // i = i + 1;
		--i;     // i = i - 1;
		i *= 2;  // i = i * 2;
		i /= 2;  // i = i / 2;
		System.out.println("i : " + i);
		System.out.println("bool : " + bool);

		i = ++i + (++i) * i;

		System.out.println("2 + (3 * 4) = " + i);

		i = 2;

		bool = (i > 5) && (i>3) ||  (i++ < 3);

		System.out.println("i : " + i);
		System.out.println("bool : " + bool);


		i = 0;

		int result = ++i + (++i * ++i);

		System.out.println("result = " + result);

		i = 1;
		if (i == 0){
			int a = 1;
			//int i = 7;
			i++;
			i++;
		}
		int a = 1;
		a++;


		System.out.println("i = " + i);

	}
	
}